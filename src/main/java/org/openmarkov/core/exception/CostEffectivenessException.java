package org.openmarkov.core.exception;

@SuppressWarnings("serial")
public class CostEffectivenessException extends Exception {

	// Constructor
	public CostEffectivenessException(String message) {
		super(message);
	}

	public CostEffectivenessException(String message, Throwable cause) {
		super(message, cause);
	}

}
