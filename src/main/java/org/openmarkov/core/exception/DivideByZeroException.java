package org.openmarkov.core.exception;

@SuppressWarnings("serial")
public class DivideByZeroException extends Exception {

	// Constructor
	/** @param message <code>String</code> */
	public DivideByZeroException(String message) {
		super(message);
	}

}
