package org.openmarkov.core.dt;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	DecisionTreeTest.class
})


public class DtTests {

}
