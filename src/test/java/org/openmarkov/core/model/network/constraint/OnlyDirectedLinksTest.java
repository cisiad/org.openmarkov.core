/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.core.model.network.constraint;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.action.AddLinkEdit;
import org.openmarkov.core.action.PNESupport;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;


public class OnlyDirectedLinksTest {

	private ProbNet probNetMixed;
	
	private ProbNet probNetUndirected;
	
	private ProbNet probNetDirected;
	
	@Before
	public void setUp() throws Exception {
		probNetMixed = ConstraintsTests.getTestProbNetMixed();
		probNetUndirected = ConstraintsTests.getTestProbNetUndirected();
		probNetDirected = ConstraintsTests.getTestProbNetDirected();
	}

	/** Checks or not all the <code>probNet</code> in different situations in
	 * <code>OnlyDirectedLinks</code> constructor. */
	public void testCheckProbNet() {
		// test only directed links insertions without checking.
	    PNConstraint constraint = new OnlyDirectedLinks(); 
		boolean exceptionLaunched = false;
		try {
			probNetMixed.addConstraint(constraint, true);
		} catch (Exception e1) {
			exceptionLaunched = true;
		}
		assertTrue(exceptionLaunched);

		probNetMixed.removeConstraint(constraint);
		exceptionLaunched = false;	
		assertFalse(constraint.checkProbNet(probNetUndirected));
		
		// test only directed links insertions with checking.
		assertFalse(constraint.checkProbNet(probNetMixed));
	}

	/** Checks veto */
	@Test
	public void testUndoableEditWillHappen() 
	        throws Exception {
		
		// Add constraints as listeners.
		PNESupport pNESupport = new PNESupport(false);
		probNetDirected.addConstraint(new OnlyDirectedLinks(), true);
		List<PNConstraint> constraints = probNetDirected.getConstraints();
		for (PNConstraint constraint : constraints) { // sets listeners
			pNESupport.addUndoableEditListener(constraint);
		}
		// Create edits
		Variable va = probNetDirected.getVariable("A");
		Variable vc = probNetDirected.getVariable("C");

		// test no exception in legal edit
		AddLinkEdit cEdit = new AddLinkEdit(probNetDirected, va, vc, true);
		try {
			pNESupport.announceEdit(cEdit);
			cEdit.doEdit();
		} catch (Exception cve) {
			fail(cve.getMessage());
		}
		
		// test exception in no legal edit
		boolean exceptionLaunched = false;
		AddLinkEdit iEdit;
		try {
            iEdit = new AddLinkEdit(probNetDirected, va, vc, false);
            pNESupport.announceEdit(iEdit);
		} catch (Exception cve) {
			exceptionLaunched = true;
		}
		assertTrue(exceptionLaunched); // pNESupport must launch an exception.
	}

}
