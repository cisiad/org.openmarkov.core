/*
 * Copyright 2011 CISIAD, UNED, Spain
 *
 * Licensed under the European Union Public Licence, version 1.1 (EUPL)
 *
 * Unless required by applicable law, this code is distributed
 * on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */
package org.openmarkov.core.model.network.modelUncertainty;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author manolo
 * 
 */
@Ignore
public abstract class FamilyDistributionTest {

    private FamilyDistribution family;

    private double             maxErrorMean        = 0.001;
    private double             maxErrorStDeviation = 0.01;

	//@Test
	public void testMeanAndVariance() {

		int numSamples = 1000000;
		Random randomGenerator = new XORShiftRandom();
		List<UncertainValue> list = initializeListUncertainValues();
		family = newFamilyDistribution(list);

		List<double[]> samples = new ArrayList<>();
		for (int i = 0; i < numSamples; i++) {
			samples.add(family.getSample(randomGenerator));
		}
		testMean(samples);
		testStandardDeviation(samples);
	}
    
	@Test
	public void repeatTestMeanAndVariance() {
		boolean debug = false;
		int numRepetitions = debug?10:1;

		for (int iRepetition = 0; iRepetition < numRepetitions; iRepetition++) {
			testMeanAndVariance();
			if (debug){
				System.out.println("iRepetition= " + iRepetition);
			}
		}
	}

	public abstract FamilyDistribution newFamilyDistribution(List<UncertainValue> list);

    /**
     * @return
     */
	public abstract List<UncertainValue> initializeListUncertainValues();

    /**
     * @param samples
     */
    protected void testMean(List<double[]> samples) {

        int numChildrenFam = samples.get(0).length;
        double[] auxSamples;
        double[] meanSample;
        meanSample = new double[numChildrenFam];
        for (int i = 0; i < numChildrenFam; i++) {
            auxSamples = new double[samples.size()];
            for (int j = 0; j < samples.size(); j++) {
                auxSamples[j] = samples.get(j)[i];
            }
            meanSample[i] = Tools.meanSample(auxSamples);
        }

        assertMeanTest(meanSample, family.getMean(), maxErrorMean);

    }

    /**
     * @param samples
     */
    protected void testStandardDeviation(List<double[]> samples) {
        int numChildrenFam = samples.get(0).length;
        double[] auxSamples;
        double[] stDSample;
        stDSample = new double[numChildrenFam];
        for (int i = 0; i < numChildrenFam; i++) {
            auxSamples = new double[samples.size()];
            for (int j = 0; j < samples.size(); j++) {
                auxSamples[j] = samples.get(j)[i];
            }
            stDSample[i] = Math.sqrt(Tools.varianceSample(auxSamples));
        }
        assertMeanTest(stDSample, family.getStandardDeviation(), maxErrorStDeviation);

    }

    /**
     * @param meanSample
     * @param meanFamily
     * @param maxErrorMean2
     */
    protected void assertMeanTest(double[] meanSample, double[] meanFamily, double maxErrorMean2) {

        for (int i = 0; i < meanSample.length; i++) {
            assertEquals(meanSample[i], meanFamily[i], maxErrorMean2);
        }
    }
}
