package org.openmarkov.core.model.network.modelUncertainty;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	BetaFunctionTest.class,
	DirichletFamilyTest.class,
	ErlangFunctionTest.class,
	ExactFunctionTest.class,
	ExponentialFunctionTest.class,
	FamilyDistributionTest.class,
	GammaFunctionTest.class,
	GammamvFunctionTest.class,
	LogNormalFunctionTest.class,
	NormalFunctionTest.class,
	ProbDensFunctionTest.class,
	RangeFunctionTest.class,
	StandardNormalFunctionTest.class,
	TriangularFunctionTest.class
})

public class ModelUncertaintyTests {

}
