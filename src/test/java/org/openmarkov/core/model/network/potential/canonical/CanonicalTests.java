package org.openmarkov.core.model.network.potential.canonical;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
	MaxPotentialTest.class,
	MinPotentialTest.class,
	TuningPotentialTest.class
})

public class CanonicalTests {

}
