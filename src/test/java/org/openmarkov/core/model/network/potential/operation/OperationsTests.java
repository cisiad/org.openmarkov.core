/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.core.model.network.potential.operation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	AuxiliaryOperationsTest.class,
	DiscretePotentialOperationsTest.class,
	LinkRestrictionPotentialOperationsTest.class,
	UtilTest.class,
    
})
        
/** @author manuel */
public class OperationsTests {

}

