package org.openmarkov.core.model.network.potential.plugin;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;



@RunWith(Suite.class)
@Suite.SuiteClasses({
	PotentialManagerTest.class
})

public class PluginTests {

}
