package org.openmarkov.core.model.network.potential.treeADD;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
	NumericalTreeADDTableProjectTest.class,
	TreeADDPotentialTest.class,
	TreeADDTableProjectTest.class
})

public class TreeAddTests {

}
