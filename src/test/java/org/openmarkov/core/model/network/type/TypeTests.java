package org.openmarkov.core.model.network.type;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;



@RunWith(Suite.class)
@Suite.SuiteClasses({
	NetworkTypeTest.class
})

public class TypeTests {

}
