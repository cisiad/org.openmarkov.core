//	   Bayesian Network
//	   Elvira format

bnet Grid_6_6 {

//		 Network Properties

default node states = (Presente, Ausente );

node Grid3_3(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =720;
pos_y =540;
//num-states = 2;
}

node Grid0_1(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =320;
pos_y =150;
//num-states = 2;
}

node Column5(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1200;
pos_y =890;
//num-states = 2;
}

node Grid1_2(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =520;
pos_y =280;
//num-states = 2;
}

node Grid5_2(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =520;
pos_y =800;
//num-states = 2;
}

node Column2(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =600;
pos_y =890;
//num-states = 2;
}

node Row3(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1320;
pos_y =500;
//num-states = 2;
}

node Grid0_2(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =520;
pos_y =150;
//num-states = 2;
}

node Grid1_3(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =720;
pos_y =280;
//num-states = 2;
}

node Row4(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1320;
pos_y =630;
//num-states = 2;
}

node Grid2_0(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =120;
pos_y =410;
//num-states = 2;
}

node Grid4_4(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =920;
pos_y =670;
//num-states = 2;
}

node Grid5_5(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1120;
pos_y =800;
//num-states = 2;
}

node Grid5_4(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =920;
pos_y =800;
//num-states = 2;
}

node Grid4_3(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =720;
pos_y =670;
//num-states = 2;
}

node Grid0_3(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =720;
pos_y =150;
//num-states = 2;
}

node Grid2_3(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =720;
pos_y =410;
//num-states = 2;
}

node Grid1_4(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =920;
pos_y =280;
//num-states = 2;
}

node Grid0_5(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1120;
pos_y =150;
//num-states = 2;
}

node Grid0_0(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =120;
pos_y =150;
//num-states = 2;
}

node Column0(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =200;
pos_y =890;
//num-states = 2;
}

node Grid2_2(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =520;
pos_y =410;
//num-states = 2;
}

node Row2(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1320;
pos_y =370;
//num-states = 2;
}

node Row0(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1320;
pos_y =110;
//num-states = 2;
}

node Grid1_1(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =320;
pos_y =280;
//num-states = 2;
}

node Grid1_5(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1120;
pos_y =280;
//num-states = 2;
}

node Grid4_0(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =120;
pos_y =670;
//num-states = 2;
}

node Grid2_1(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =320;
pos_y =410;
//num-states = 2;
}

node Grid3_4(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =920;
pos_y =540;
//num-states = 2;
}

node Grid3_5(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1120;
pos_y =540;
//num-states = 2;
}

node Grid5_3(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =720;
pos_y =800;
//num-states = 2;
}

node Grid4_2(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =520;
pos_y =670;
//num-states = 2;
}

node Grid2_4(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =920;
pos_y =410;
//num-states = 2;
}

node Grid3_2(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =520;
pos_y =540;
//num-states = 2;
}

node Grid3_1(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =320;
pos_y =540;
//num-states = 2;
}

node Grid5_0(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =120;
pos_y =800;
//num-states = 2;
}

node Row5(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1320;
pos_y =760;
//num-states = 2;
}

node Row1(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1320;
pos_y =240;
//num-states = 2;
}

node Column3(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =800;
pos_y =890;
//num-states = 2;
}

node Grid2_5(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1120;
pos_y =410;
//num-states = 2;
}

node Grid4_5(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1120;
pos_y =670;
//num-states = 2;
}

node Grid4_1(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =320;
pos_y =670;
//num-states = 2;
}

node Column4(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =1000;
pos_y =890;
//num-states = 2;
}

node Grid3_0(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =120;
pos_y =540;
//num-states = 2;
}

node Column1(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =400;
pos_y =890;
//num-states = 2;
}

node Grid5_1(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =320;
pos_y =800;
//num-states = 2;
}

node Grid1_0(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =120;
pos_y =280;
//num-states = 2;
}

node Grid0_4(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =920;
pos_y =150;
//num-states = 2;
}

//		 Links of the associated graph:

link Grid0_0 Row0;
link Grid0_0 Column0;
link Grid0_1 Row0;
link Grid0_1 Column1;
link Grid0_2 Row0;
link Grid0_2 Column2;
link Grid0_3 Row0;
link Grid0_3 Column3;
link Grid0_4 Row0;
link Grid0_4 Column4;
link Grid0_5 Row0;
link Grid0_5 Column5;
link Grid1_0 Row1;
link Grid1_0 Column0;
link Grid1_1 Row1;
link Grid1_1 Column1;
link Grid1_2 Row1;
link Grid1_2 Column2;
link Grid1_3 Row1;
link Grid1_3 Column3;
link Grid1_4 Row1;
link Grid1_4 Column4;
link Grid1_5 Row1;
link Grid1_5 Column5;
link Grid2_0 Row2;
link Grid2_0 Column0;
link Grid2_1 Row2;
link Grid2_1 Column1;
link Grid2_2 Row2;
link Grid2_2 Column2;
link Grid2_3 Row2;
link Grid2_3 Column3;
link Grid2_4 Row2;
link Grid2_4 Column4;
link Grid2_5 Row2;
link Grid2_5 Column5;
link Grid3_0 Row3;
link Grid3_0 Column0;
link Grid3_1 Row3;
link Grid3_1 Column1;
link Grid3_2 Row3;
link Grid3_2 Column2;
link Grid3_3 Row3;
link Grid3_3 Column3;
link Grid3_4 Row3;
link Grid3_4 Column4;
link Grid3_5 Row3;
link Grid3_5 Column5;
link Grid4_0 Row4;
link Grid4_0 Column0;
link Grid4_1 Row4;
link Grid4_1 Column1;
link Grid4_2 Row4;
link Grid4_2 Column2;
link Grid4_3 Row4;
link Grid4_3 Column3;
link Grid4_4 Row4;
link Grid4_4 Column4;
link Grid4_5 Row4;
link Grid4_5 Column5;
link Grid5_0 Row5;
link Grid5_0 Column0;
link Grid5_1 Row5;
link Grid5_1 Column1;
link Grid5_2 Row5;
link Grid5_2 Column2;
link Grid5_3 Row5;
link Grid5_3 Column3;
link Grid5_4 Row5;
link Grid5_4 Column4;
link Grid5_5 Row5;
link Grid5_5 Column5;

//		Network Relationships:

relation Grid0_0 {
values = table(0.3, 0.7);
}

relation Grid0_1 {
values = table(0.42, 0.58);
}

relation Grid0_2 {
values = table(0.8, 0.2);
}

relation Grid0_3 {
values = table(0.17, 0.83);
}

relation Grid0_4 {
values = table(0.56, 0.44);
}

relation Grid0_5 {
values = table(0.51, 0.49);
}

relation Grid1_0 {
values = table(0.12, 0.88);
}

relation Grid1_1 {
values = table(0.99, 0.01);
}

relation Grid1_2 {
values = table(0.94, 0.06);
}

relation Grid1_3 {
values = table(0.26, 0.74);
}

relation Grid1_4 {
values = table(0.3, 0.7);
}

relation Grid1_5 {
values = table(0.41, 0.59);
}

relation Grid2_0 {
values = table(0.5, 0.5);
}

relation Grid2_1 {
values = table(0.41, 0.59);
}

relation Grid2_2 {
values = table(0.47, 0.53);
}

relation Grid2_3 {
values = table(0.67, 0.33);
}

relation Grid2_4 {
values = table(0.37, 0.63);
}

relation Grid2_5 {
values = table(0.19, 0.81);
}

relation Grid3_0 {
values = table(0.77, 0.23);
}

relation Grid3_1 {
values = table(0.19, 0.81);
}

relation Grid3_2 {
values = table(0.76, 0.24);
}

relation Grid3_3 {
values = table(0.99, 0.01);
}

relation Grid3_4 {
values = table(0.02, 0.98);
}

relation Grid3_5 {
values = table(0.57, 0.43);
}

relation Grid4_0 {
values = table(0.33, 0.67);
}

relation Grid4_1 {
values = table(0.45, 0.55);
}

relation Grid4_2 {
values = table(0.63, 0.37);
}

relation Grid4_3 {
values = table(0.55, 0.45);
}

relation Grid4_4 {
values = table(0.65, 0.35);
}

relation Grid4_5 {
values = table(0.54, 0.46);
}

relation Grid5_0 {
values = table(0.2, 0.8);
}

relation Grid5_1 {
values = table(0.43, 0.57);
}

relation Grid5_2 {
values = table(0.36, 0.64);
}

relation Grid5_3 {
values = table(0.42, 0.58);
}

relation Grid5_4 {
values = table(0.69, 0.31);
}

relation Grid5_5 {
values = table(0.83, 0.17);
}

relation Row0 Grid0_5 Grid0_4 Grid0_3 Grid0_2 Grid0_1 Grid0_0 {
values = table(0.43, 0.74, 0.37, 0.9, 0.91, 0.23, 0.8, 0.06, 0.41, 0.81, 0.96, 0.69, 0.34, 0.96, 0.45, 0.94, 0.43, 0.24, 0.26, 0.72, 
0.92, 0.69, 0.53, 0.53, 0.86, 0.87, 0.61, 0.24, 0.61, 0.13, 0.04, 0.27, 0.94, 0.37, 0.56, 0.56, 0.13, 0.73, 0.25, 0.93, 
0.76, 0.73, 0.8, 0.17, 0.17, 0.34, 0.91, 0.31, 0.43, 0.53, 0.94, 0.55, 0.23, 0.91, 0.36, 0.25, 0.37, 0.56, 0.33, 0.19, 
0.97, 0.62, 0.66, 0.14, 0.57, 0.26, 0.63, 0.1, 0.09, 0.77, 0.2, 0.94, 0.59, 0.19, 0.04, 0.31, 0.66, 0.04, 0.55, 0.06, 
0.57, 0.76, 0.74, 0.28, 0.08, 0.31, 0.47, 0.47, 0.14, 0.13, 0.39, 0.76, 0.39, 0.87, 0.96, 0.73, 0.06, 0.63, 0.44, 0.44, 
0.87, 0.27, 0.75, 0.07, 0.24, 0.27, 0.2, 0.83, 0.83, 0.66, 0.09, 0.69, 0.57, 0.47, 0.06, 0.45, 0.77, 0.09, 0.64, 0.75, 
0.63, 0.44, 0.67, 0.81, 0.03, 0.38, 0.34, 0.86);
}

relation Row1 Grid1_5 Grid1_4 Grid1_3 Grid1_2 Grid1_1 Grid1_0 {
values = table(0.14, 0.77, 0.9, 0.6, 0.17, 0.57, 0.86, 0.46, 0.54, 0.14, 0.02, 0.06, 0.19, 0.46, 0.14, 0.89, 0.66, 0.92, 0.71, 0.7, 
0.35, 0.34, 0.46, 0.03, 0.26, 0.2, 0.29, 0.73, 0.34, 0.02, 0.53, 0.11, 0.42, 0.08, 0.92, 0.31, 0.19, 0.5, 0.23, 0.09, 
0.24, 0.53, 0.41, 0.97, 0.73, 0.31, 0.95, 0.59, 0.57, 0.38, 0.69, 0.57, 0.3, 0.69, 0.74, 0.21, 0.8, 0.19, 0.68, 0.61, 
0.88, 0.8, 0.46, 0.29, 0.86, 0.23, 0.1, 0.4, 0.83, 0.43, 0.14, 0.54, 0.46, 0.86, 0.98, 0.94, 0.81, 0.54, 0.86, 0.11, 
0.34, 0.08, 0.29, 0.3, 0.65, 0.66, 0.54, 0.97, 0.74, 0.8, 0.71, 0.27, 0.66, 0.98, 0.47, 0.89, 0.58, 0.92, 0.08, 0.69, 
0.81, 0.5, 0.77, 0.91, 0.76, 0.47, 0.59, 0.03, 0.27, 0.69, 0.05, 0.41, 0.43, 0.62, 0.31, 0.43, 0.7, 0.31, 0.26, 0.79, 
0.2, 0.81, 0.32, 0.39, 0.12, 0.2, 0.54, 0.71);
}

relation Row2 Grid2_5 Grid2_4 Grid2_3 Grid2_2 Grid2_1 Grid2_0 {
values = table(0.08, 0.18, 0.49, 0.8, 0.79, 0.37, 0.27, 0.96, 0.49, 0.42, 0.16, 0.89, 0.9, 0.03, 0.21, 0.71, 0.31, 0.1, 0.9, 0.24, 
0.14, 0.72, 0.22, 0.67, 0.66, 0.08, 0.44, 0.37, 0.69, 0.97, 0.96, 0.78, 0.02, 0.19, 0.29, 0.69, 0.05, 0.2, 0.92, 0.19, 
0.3, 0.91, 0.8, 0.83, 0.61, 0.51, 0.77, 0.11, 0.39, 0.38, 0.33, 0.21, 0.75, 0.37, 0.48, 0.43, 0.35, 0.05, 0.53, 0.79, 
0.46, 0.65, 0.95, 0.47, 0.92, 0.82, 0.51, 0.2, 0.21, 0.63, 0.73, 0.04, 0.51, 0.58, 0.84, 0.11, 0.1, 0.97, 0.79, 0.29, 
0.69, 0.9, 0.1, 0.76, 0.86, 0.28, 0.78, 0.33, 0.34, 0.92, 0.56, 0.63, 0.31, 0.03, 0.04, 0.22, 0.98, 0.81, 0.71, 0.31, 
0.95, 0.8, 0.08, 0.81, 0.7, 0.09, 0.2, 0.17, 0.39, 0.49, 0.23, 0.89, 0.61, 0.62, 0.67, 0.79, 0.25, 0.63, 0.52, 0.57, 
0.65, 0.95, 0.47, 0.21, 0.54, 0.35, 0.05, 0.53);
}

relation Row3 Grid3_5 Grid3_4 Grid3_3 Grid3_2 Grid3_1 Grid3_0 {
values = table(0.76, 0.94, 0.4, 0.35, 0.14, 0.98, 0.96, 0.59, 0.94, 0.6, 0.76, 0.68, 0.82, 0.51, 0.69, 0.32, 0.31, 0.14, 0.12, 0.71, 
0.38, 0.51, 0.54, 0.09, 0.53, 0.47, 0.27, 0.66, 0.85, 0.61, 0.4, 0.68, 0.95, 0.42, 0.07, 0.98, 0.29, 0.02, 0.52, 0.83, 
0.61, 0.35, 0.69, 0.96, 0.15, 0.28, 0.35, 0.24, 0.56, 0.27, 0.62, 0.86, 0.34, 0.83, 0.92, 0.84, 0.38, 0.44, 0.87, 0.93, 
0.67, 0.24, 0.51, 0.21, 0.24, 0.06, 0.6, 0.65, 0.86, 0.02, 0.04, 0.41, 0.06, 0.4, 0.24, 0.32, 0.18, 0.49, 0.31, 0.68, 
0.69, 0.86, 0.88, 0.29, 0.62, 0.49, 0.46, 0.91, 0.47, 0.53, 0.73, 0.34, 0.15, 0.39, 0.6, 0.32, 0.05, 0.58, 0.93, 0.02, 
0.71, 0.98, 0.48, 0.17, 0.39, 0.65, 0.31, 0.04, 0.85, 0.72, 0.65, 0.76, 0.44, 0.73, 0.38, 0.14, 0.66, 0.17, 0.08, 0.16, 
0.62, 0.56, 0.13, 0.07, 0.33, 0.76, 0.49, 0.79);
}

relation Row4 Grid4_5 Grid4_4 Grid4_3 Grid4_2 Grid4_1 Grid4_0 {
values = table(0.23, 0.86, 0.78, 0.76, 0.16, 0.38, 0.51, 0.56, 0.69, 0.21, 0.39, 0.37, 0.78, 0.99, 0.55, 0.52, 0.28, 0.23, 0.43, 0.71, 
0.22, 0.04, 0.82, 0.91, 0.54, 0.92, 0.69, 0.15, 0.1, 0.73, 0.39, 0.35, 0.06, 0.07, 0.18, 0.27, 0.87, 0.39, 0.3, 0.5, 
0.03, 0.36, 0.55, 0.05, 0.9, 0.11, 0.37, 0.2, 0.35, 0.76, 0.54, 0.37, 0.24, 0.3, 0.16, 0.49, 0.08, 0.67, 0.61, 0.7, 
0.44, 0.16, 0.58, 0.5, 0.77, 0.14, 0.22, 0.24, 0.84, 0.62, 0.49, 0.44, 0.31, 0.79, 0.61, 0.63, 0.22, 0.01, 0.45, 0.48, 
0.72, 0.77, 0.57, 0.29, 0.78, 0.96, 0.18, 0.09, 0.46, 0.08, 0.31, 0.85, 0.9, 0.27, 0.61, 0.65, 0.94, 0.93, 0.82, 0.73, 
0.13, 0.61, 0.7, 0.5, 0.97, 0.64, 0.45, 0.95, 0.1, 0.89, 0.63, 0.8, 0.65, 0.24, 0.46, 0.63, 0.76, 0.7, 0.84, 0.51, 
0.92, 0.33, 0.39, 0.3, 0.56, 0.84, 0.42, 0.5);
}

relation Row5 Grid5_5 Grid5_4 Grid5_3 Grid5_2 Grid5_1 Grid5_0 {
values = table(0.92, 0.81, 0.6, 0.22, 0.11, 0.95, 0.33, 0.7, 0.8, 0.66, 0.03, 0.94, 0.86, 0.11, 0.51, 0.39, 0.9, 0.63, 0.04, 0.44, 
0.35, 0.14, 0.53, 0.58, 0.13, 0.8, 0.44, 0.05, 0.18, 0.52, 0.23, 0.15, 0.27, 0.8, 0.95, 0.74, 0.89, 0.45, 0.62, 0.42, 
0.15, 0.04, 0.1, 0.85, 0.04, 0.85, 0.85, 0.5, 0.12, 0.31, 0.88, 0.67, 0.46, 0.79, 0.36, 0.09, 0.92, 0.68, 0.54, 0.53, 
0.88, 0.12, 0.78, 0.19, 0.08, 0.19, 0.4, 0.78, 0.89, 0.05, 0.67, 0.3, 0.2, 0.34, 0.97, 0.06, 0.14, 0.89, 0.49, 0.61, 
0.1, 0.37, 0.96, 0.56, 0.65, 0.86, 0.47, 0.42, 0.87, 0.2, 0.56, 0.95, 0.82, 0.48, 0.77, 0.85, 0.73, 0.2, 0.05, 0.26, 
0.11, 0.55, 0.38, 0.58, 0.85, 0.96, 0.9, 0.15, 0.96, 0.15, 0.15, 0.5, 0.88, 0.69, 0.12, 0.33, 0.54, 0.21, 0.64, 0.91, 
0.08, 0.32, 0.46, 0.47, 0.12, 0.88, 0.22, 0.81);
}

relation Column0 Grid5_0 Grid4_0 Grid3_0 Grid2_0 Grid1_0 Grid0_0 {
values = table(0.13, 0.09, 0.71, 0.9, 0.13, 0.14, 0.84, 0.06, 0.43, 0.97, 0.22, 0.5, 0.32, 0.37, 0.49, 0.67, 0.66, 0.46, 0.74, 0.46, 
0.37, 0.06, 0.88, 0.27, 0.72, 0.62, 0.69, 0.78, 0.17, 0.21, 0.29, 0.59, 0.69, 0.73, 0.2, 0.26, 0.02, 0.91, 0.36, 0.63, 
0.73, 0.31, 0.92, 0.77, 0.43, 0.62, 0.29, 0.89, 0.74, 0.31, 0.07, 0.34, 0.29, 0.59, 0.27, 0.66, 0.89, 0.53, 0.18, 0.88, 
0.62, 0.09, 0.08, 0.07, 0.87, 0.91, 0.29, 0.1, 0.87, 0.86, 0.16, 0.94, 0.57, 0.03, 0.78, 0.5, 0.68, 0.63, 0.51, 0.33, 
0.34, 0.54, 0.26, 0.54, 0.63, 0.94, 0.12, 0.73, 0.28, 0.38, 0.31, 0.22, 0.83, 0.79, 0.71, 0.41, 0.31, 0.27, 0.8, 0.74, 
0.98, 0.09, 0.64, 0.37, 0.27, 0.69, 0.08, 0.23, 0.57, 0.38, 0.71, 0.11, 0.26, 0.69, 0.93, 0.66, 0.71, 0.41, 0.73, 0.34, 
0.11, 0.47, 0.82, 0.12, 0.38, 0.91, 0.92, 0.93);
}

relation Column1 Grid5_1 Grid4_1 Grid3_1 Grid2_1 Grid1_1 Grid0_1 {
values = table(0.36, 0.57, 0.25, 0.07, 0.78, 0.39, 0.62, 0.12, 0.14, 0.83, 0.67, 0.21, 0.8, 0.98, 0.13, 0.85, 0.33, 0.61, 0.27, 0.18, 
0.7, 0.23, 0.91, 0.94, 0.45, 0.75, 0.08, 0.37, 0.2, 0.89, 0.4, 0.47, 0.35, 0.32, 0.68, 0.57, 0.62, 0.91, 0.37, 0.63, 
0.81, 0.33, 0.61, 0.95, 0.64, 0.18, 0.28, 0.25, 0.72, 0.13, 0.21, 0.32, 0.5, 0.25, 0.23, 0.83, 0.67, 0.35, 0.27, 0.43, 
0.22, 0.18, 0.82, 0.48, 0.64, 0.43, 0.75, 0.93, 0.22, 0.61, 0.38, 0.88, 0.86, 0.17, 0.33, 0.79, 0.2, 0.02, 0.87, 0.15, 
0.67, 0.39, 0.73, 0.82, 0.3, 0.77, 0.09, 0.06, 0.55, 0.25, 0.92, 0.63, 0.8, 0.11, 0.6, 0.53, 0.65, 0.68, 0.32, 0.43, 
0.38, 0.09, 0.63, 0.37, 0.19, 0.67, 0.39, 0.05, 0.36, 0.82, 0.72, 0.75, 0.28, 0.87, 0.79, 0.68, 0.5, 0.75, 0.77, 0.17, 
0.33, 0.65, 0.73, 0.57, 0.78, 0.82, 0.18, 0.52);
}

relation Column2 Grid5_2 Grid4_2 Grid3_2 Grid2_2 Grid1_2 Grid0_2 {
values = table(0.58, 0.76, 0.3, 0.37, 0.9, 0.07, 0.69, 0.86, 0.89, 0.36, 0.74, 0.4, 0.58, 0.28, 0.96, 0.73, 0.97, 0.73, 0.2, 0.64, 
0.77, 0.85, 0.98, 0.82, 0.91, 0.77, 0.21, 0.67, 0.65, 0.13, 0.54, 0.58, 0.23, 0.7, 0.51, 0.43, 0.71, 0.74, 0.68, 0.31, 
0.95, 0.26, 0.42, 0.19, 0.38, 0.78, 0.05, 0.27, 0.88, 0.16, 0.6, 0.89, 0.43, 0.9, 0.02, 0.5, 0.04, 0.51, 0.66, 0.81, 
0.84, 0.8, 0.61, 0.99, 0.42, 0.24, 0.7, 0.63, 0.1, 0.93, 0.31, 0.14, 0.11, 0.64, 0.26, 0.6, 0.42, 0.72, 0.04, 0.27, 
0.03, 0.27, 0.8, 0.36, 0.23, 0.15, 0.02, 0.18, 0.09, 0.23, 0.79, 0.33, 0.35, 0.87, 0.46, 0.42, 0.77, 0.3, 0.49, 0.57, 
0.29, 0.26, 0.32, 0.69, 0.05, 0.74, 0.58, 0.81, 0.62, 0.22, 0.95, 0.73, 0.12, 0.84, 0.4, 0.11, 0.57, 0.1, 0.98, 0.5, 
0.96, 0.49, 0.34, 0.19, 0.16, 0.2, 0.39, 0.01);
}

relation Column3 Grid5_3 Grid4_3 Grid3_3 Grid2_3 Grid1_3 Grid0_3 {
values = table(0.08, 0.26, 0.8, 0.5, 0.39, 0.53, 0.06, 0.65, 0.17, 0.91, 0.19, 0.11, 0.49, 0.44, 0.1, 0.89, 0.24, 0.63, 0.43, 0.24, 
0.98, 0.16, 0.95, 0.08, 0.48, 0.97, 0.88, 0.55, 0.79, 0.13, 0.86, 0.06, 0.49, 0.08, 0.31, 0.58, 0.37, 0.72, 0.19, 0.37, 
0.74, 0.8, 0.12, 0.71, 0.64, 0.9, 0.05, 0.45, 0.32, 0.33, 0.81, 0.67, 0.8, 0.21, 0.33, 0.21, 0.08, 0.86, 0.36, 0.97, 
0.73, 0.56, 0.03, 0.18, 0.92, 0.74, 0.2, 0.5, 0.61, 0.47, 0.94, 0.35, 0.83, 0.09, 0.81, 0.89, 0.51, 0.56, 0.9, 0.11, 
0.76, 0.37, 0.57, 0.76, 0.02, 0.84, 0.05, 0.92, 0.52, 0.03, 0.12, 0.45, 0.21, 0.87, 0.14, 0.94, 0.51, 0.92, 0.69, 0.42, 
0.63, 0.28, 0.81, 0.63, 0.26, 0.2, 0.88, 0.29, 0.36, 0.1, 0.95, 0.55, 0.68, 0.67, 0.19, 0.33, 0.2, 0.79, 0.67, 0.79, 
0.92, 0.14, 0.64, 0.03, 0.27, 0.44, 0.97, 0.82);
}

relation Column4 Grid5_4 Grid4_4 Grid3_4 Grid2_4 Grid1_4 Grid0_4 {
values = table(0.59, 0.44, 0.36, 0.21, 0.25, 0.67, 0.41, 0.93, 0.33, 0.22, 0.69, 0.59, 0.2, 0.47, 0.58, 0.06, 0.26, 0.13, 0.87, 0.75, 
0.37, 0.46, 0.07, 0.28, 0.33, 0.07, 0.34, 0.67, 0.2, 0.34, 0.23, 0.36, 0.38, 0.32, 0.77, 0.33, 0.16, 0.96, 0.34, 0.48, 
0.27, 0.85, 0.6, 0.93, 0.78, 0.94, 0.91, 0.7, 0.71, 0.84, 0.47, 0.7, 0.74, 0.79, 0.03, 0.95, 0.29, 0.22, 0.9, 0.83, 
0.05, 0.82, 0.66, 0.09, 0.41, 0.56, 0.64, 0.79, 0.75, 0.33, 0.59, 0.07, 0.67, 0.78, 0.31, 0.41, 0.8, 0.53, 0.42, 0.94, 
0.74, 0.87, 0.13, 0.25, 0.63, 0.54, 0.93, 0.72, 0.67, 0.93, 0.66, 0.33, 0.8, 0.66, 0.77, 0.64, 0.62, 0.68, 0.23, 0.67, 
0.84, 0.04, 0.66, 0.52, 0.73, 0.15, 0.4, 0.07, 0.22, 0.06, 0.09, 0.3, 0.29, 0.16, 0.53, 0.3, 0.26, 0.21, 0.97, 0.05, 
0.71, 0.78, 0.1, 0.17, 0.95, 0.18, 0.34, 0.91);
}

relation Column5 Grid5_5 Grid4_5 Grid3_5 Grid2_5 Grid1_5 Grid0_5 {
values = table(0.32, 0.25, 0.21, 0.77, 0.09, 0.5, 0.67, 0.85, 0.96, 0.41, 0.37, 0.42, 0.8, 0.35, 0.31, 0.58, 0.37, 0.98, 0.33, 0.78, 
0.56, 0.23, 0.5, 0.66, 0.54, 0.3, 0.85, 0.1, 0.74, 0.09, 0.8, 0.25, 0.12, 0.9, 0.34, 0.59, 0.73, 0.15, 0.08, 0.62, 
0.07, 0.36, 0.36, 0.89, 0.59, 0.98, 0.87, 0.06, 0.93, 0.67, 0.97, 0.65, 0.08, 0.3, 0.85, 0.04, 0.51, 0.18, 0.42, 0.79, 
0.56, 0.35, 0.52, 0.99, 0.68, 0.75, 0.79, 0.23, 0.91, 0.5, 0.33, 0.15, 0.04, 0.59, 0.63, 0.58, 0.2, 0.65, 0.69, 0.42, 
0.63, 0.02, 0.67, 0.22, 0.44, 0.77, 0.5, 0.34, 0.46, 0.7, 0.15, 0.9, 0.26, 0.91, 0.2, 0.75, 0.88, 0.1, 0.66, 0.41, 
0.27, 0.85, 0.92, 0.38, 0.93, 0.64, 0.64, 0.11, 0.41, 0.02, 0.13, 0.94, 0.07, 0.33, 0.03, 0.35, 0.92, 0.7, 0.15, 0.96, 
0.49, 0.82, 0.58, 0.21, 0.44, 0.65, 0.48, 0.01);
}

}

